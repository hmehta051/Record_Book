import axios from 'axios'
import React, { useState } from 'react'
import NormalTr from './NormalTr'
import EditTr from './EditTr'

function MapData({data, setData}) {
    const [seen,setSeen]= useState(-1)
    const handleEdit=(id)=>{
        setSeen(id)
    }
    const setNewData = (item) =>{
        axios.patch(`http://localhost:3001/students/${item.id}` , item )
        .then( res =>{
            console.log(res)
        })
        let newArr = data.map( a =>{
            if( a.id == item.id){
                a = item;
            }
            return a
        })
        setData([...newArr])
        // console.log( "testing" , item)
    }
  return (
    <>
        {
            data.map( a =>{
                return (<tr key={a.id} onClick={() => handleEdit(a.id)}>
                    { seen == a.id ?
                        <EditTr handleEdit={handleEdit} item={a} setNewData={setNewData} />
                        :
                        <NormalTr  handleEdit={handleEdit} item={a}/> 
                    }
                      </tr>)   
            }) 
        }
    </>
  )
}
export default MapData